import java.util.Scanner;

public class VirtualPetApp{
	public static void main(String[] args){
		
		Scanner reader = new Scanner(System.in);
		
		Animal[] animals = new Animal[4];
		
		for (int i = 0; i < animals.length; i++){
			System.out.println("What food does it eat?");
			String food = reader.nextLine();
			
			System.out.println("Whats its size in feet ?");
			double size = reader.nextDouble();
			
			System.out.println("Does it fly? true or false");
			boolean flies = reader.nextBoolean();
			reader.nextLine();
			animals[i] = new Animal(food, size, flies);	
		}
		
		System.out.println(animals[3].food);
		System.out.println(animals[3].size);
		System.out.println(animals[3].flies);
		
		System.out.println(animals[0].favFood());
		System.out.println(animals[0].fliesOrNot());
	}
}