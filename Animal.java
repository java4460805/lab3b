public class Animal{
	public String food;
	public double size;
	public boolean flies;
	
	public Animal(String f, double s, boolean y){
		food = f;
		size = s;
		flies = y;
	}
	
	public String favFood(){
		return "Hi I'm a dragon and I like " + food + ".";
	}
	
	public String fliesOrNot(){
		if (flies == true)
			return "Hi I'm a dragon and I can fly :)";
		else
			return "Hi I'm a dragon and I can't fly :( ";
	}
	
}